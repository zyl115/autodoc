# AutoDoc
## Overview
A python script that automates the error documentation of failed orders to JIRA and to datalake. The flow of the automated process is as follows:

1. Read from table 'missing_reasons' in datalake and extract orderIDs of the failed jobs.
2. Send chat messages to Wibot to extract logs to S3 bucket using Rocketchat API.
3. Fetch the logs from S3 Bucket using AWS SDK for Python (Boto 3).
4. Match each log file with known errors documented in the datalake. (Datalake contains the regular expression of documented errors)
5. If a known error is found, append the error to the relevant repush and root-fix tickets in JIRA using JIRA API. If it is a recurring error that has been previously resolved, a new ticket will be cloned. 
6. Update the datalake with the newly-documented failed order, along with the repush ticket, root-fix ticket, comments and error regex. 
7. Generate a report in csv format.
8. Remove all logs that are older than 5 days.
9. Email the report and logs to the recipients in mailing list. 

## Access permissions required

- Extraction of logs from wibot (Needs to be enabled by DevOps)
- Access to s3://s3.amazonaws.com/pbww-printbox/ (ID and secret key required)
- Access to 'missing_reasons' table in datalake (permission to update table) 
- JIRA access

## Set Up

1. Clone the repository
2. Install the dependencies using the following command:
```
pip3 install -r requirements.txt
```
3. Create a file `config.py` at the root directory using the template `config.py.example`, and complete the file with your credentials.
4. Run the script in debug mode to ensure everything is set up properly. (Note: this does not write to any external system). The command for running the code in debug mode:
```
python3 main.py debug
```

## Running the Script
1. Execute the script by running:
```
python3 main.py
```
2. The log file of this script can be found in directory `autodoc_logs/`. 
3. The report of each execution can be found in directory `autodox_reports/`.
4. The order logs can be found in directory `order_logs/`. 

## Other utilities
- Command to return a list of untreated orders in datalake:
```
python3 pony_orm.py
```
- Command to download log(s) from s3:
```
python3 s3.py [<orderId1> <orderId2> ...]
```
- Command to run the script in debug mode (no writing operation to extrenal systems i.e. JIRA and datalake)
```
python3 main.py debug
```

