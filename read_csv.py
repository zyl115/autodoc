from __future__ import print_function
import pickle
import os.path
import numpy as np
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
import logging

# If modifying these scopes, delete the file token.pickle.
SCOPES = ["https://www.googleapis.com/auth/spreadsheets.readonly"]

# The ID and range of a sample spreadsheet.
SAMPLE_SPREADSHEET_ID = "1VeiD6mw5K_McS_6Bj1-jpC7F6mj-QEiqaJJ396UI2js"
SAMPLE_RANGE_NAME = "Missing PBX items in Zen!A1:R1500"


def sanitizeData(excelRows):
    zeroRemoved = list(filter(None, excelRows))
    numOfColumns = max(map(lambda row: len(row), zeroRemoved))
    # Standardize number of columns for each row
    return np.array([i + [""] * (numOfColumns - len(i)) for i in zeroRemoved])


def get_failed_orders_id():
    """Shows basic usage of the Sheets API.
    Prints values from a sample spreadsheet.
    """

    logging.info("Getting orderID of failed orders...")
    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists("token.pickle"):
        with open("token.pickle", "rb") as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file("credentials.json", SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open("token.pickle", "wb") as token:
            pickle.dump(creds, token)

    service = build("sheets", "v4", credentials=creds, cache_discovery=False)

    # Call the Sheets API
    sheet = service.spreadsheets()
    result = (
        sheet.values()
        .get(spreadsheetId=SAMPLE_SPREADSHEET_ID, range=SAMPLE_RANGE_NAME)
        .execute()
    )
    values = result.get("values", [])

    if not values:
        print("No data found.")
    else:
        sanitizedData = sanitizeData(values)
        cerebroToZenOrders = list(
            filter(
                lambda order: (
                    (order[1] == "Sent to Cerebro/Zen")
                    and (order[13] == "")
                    and (order[14] == "")
                ),
                sanitizedData,
            )
        )
        orderIds = list(map(lambda row: row[0], cerebroToZenOrders))
        logging.info("Failed orderIds: {}".format(orderIds))
        print("Failed orderIds: {}".format(orderIds))
        return orderIds


if __name__ == "__main__":
    print(get_failed_orders_id())
