# Python code to illustrate Sending mail with attachments 
# from your Gmail account  

# libraries to be imported 
import smtplib 
import datetime
from email.mime.multipart import MIMEMultipart 
from email.mime.text import MIMEText 
from email.mime.base import MIMEBase 
from email import encoders
import os

def send_attachments(sender, recipients, password, files, body=""):

    now = datetime.datetime.now()

    # instance of MIMEMultipart 
    msg = MIMEMultipart() 

    # storing the senders email address   
    msg['From'] = sender

    # storing the receivers email address  
    msg['To'] = ', '.join(recipients)

    # storing the subject  
    msg['Subject'] = "Report: Failed Jobs Documentation [{}]".format(now.strftime("%Y-%m-%d"))

    # string to store the body of the mail 
    body = ""

    # attach the body with the msg instance 
    msg.attach(MIMEText(body, 'plain')) 

    if len(files)>0:
        for filepath in files:

            # open the file to be sent  
            attachment = open(filepath, "rb")
            _, filename = os.path.split(filepath)

            # instance of MIMEBase and named as p 
            p = MIMEBase('application', 'octet-stream') 

            # To change the payload into encoded form 
            p.set_payload(attachment.read()) 

            # encode into base64 
            encoders.encode_base64(p) 

            p.add_header('Content-Disposition', "attachment; filename= %s" % filename) 

            # attach the instance 'p' to instance 'msg' 
            msg.attach(p) 

    # creates SMTP session 
    s = smtplib.SMTP('smtp.gmail.com', 587) 

    # start TLS for security 
    s.starttls() 

    # Authentication 
    s.login(sender, password) 

    # Converts the Multipart msg into a string 
    text = msg.as_string() 

    # sending the mail 
    s.sendmail(sender, recipients, text) 

    # terminating the session 
    s.quit()