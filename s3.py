import boto3
import botocore
import os
import time
import logging
import re
import sys
import config as cfg
from datetime import datetime

os.environ["AWS_ACCESS_KEY_ID"] = cfg.aws["access_key_id"]
os.environ["AWS_SECRET_ACCESS_KEY"] = cfg.aws["secret_access_key"]
s3 = boto3.resource("s3")


def download_log(orderId, timeout):
    filename = "order_logs/{}.log".format(orderId)
    obj = s3.Object(
        bucket_name="pbww-cerebro2-extract-logs", key="{}.log".format(orderId)
    )
    timeout_start = time.time()
    while time.time() < timeout_start + timeout:  # 300s of timeout
        try:
            response = obj.get()
            data = response["Body"].read().decode("utf-8")
            logging.info("Log of order {} found".format(orderId))

            data_cleaned_up = list(
                filter(
                    lambda line: re.search('"orderId":{}'.format(orderId), line)
                    or re.search('"orderId":"...{}"'.format(orderId), line)
                    or re.search('"orderId":"{}"'.format(orderId), line),
                    data.splitlines(),
                )
            )
            sorted_data = sorted(
                data_cleaned_up, key=lambda line: dateTime(line), reverse=True
            )
            if not os.path.exists("order_logs"):
                os.makedirs("order_logs")
            f = open(filename, "w")
            f.write("\n".join(sorted_data))
            f.close()
            return filename
        except s3.meta.client.exceptions.NoSuchKey:
            logging.warning("Log not present in S3 bucket. Retry in 30s...")
            print("Log not present in S3 bucket. Retry in 30s...")
            time.sleep(30)
    logging.warning(
        "Timeout exceeded, log for order {} is still absent in S3. Ignoring this order.".format(
            orderId
        )
    )
    return None


def dateTime(line):
    dateString = re.search(
        r"[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}", line
    )[0]
    return datetime.strptime(dateString, "%Y-%m-%d %H:%M:%S")

if __name__ == "__main__":
    if len(sys.argv)>1:
        orderIds = sys.argv[1:]
        for orderId in orderIds:
            download_log(orderId, 300)