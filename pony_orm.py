from pony.orm import *
import logging
import config as cfg

db = Database()
db.bind(
    provider=cfg.datalake["provider"],
    host=cfg.datalake["host"],
    user=cfg.datalake["user"],
    passwd=cfg.datalake["passwd"],
    db=cfg.datalake["db"],
)


class Missing_Reason(db.Entity):
    id = PrimaryKey(int, auto=True)
    order_id = Required(int)
    no_show = Optional(int, size=8)
    repush_ticket = Optional(str)
    root_fix_ticket = Optional(str)
    comment = Optional(str)
    error_message = Optional(str)


db.generate_mapping(create_tables=False)


@db_session
def select_all():
    return select(order for order in Missing_Reason)[:]


@db_session
def extract_error_list():
    return select(
        order.error_message for order in Missing_Reason if order.error_message
    )[:]


@db_session
def select_order(orderId):
    row = list(
        select(
            (order.repush_ticket, order.root_fix_ticket, order.comment, order.no_show)
            for order in Missing_Reason
            if order.order_id == orderId
        )
    )
    if row:
        return row[0]
    else:
        return None


@db_session
def extract_repush_ticket(regex: str):
    return select(
        order.repush_ticket for order in Missing_Reason if order.error_message == regex
    )[:]


@db_session
def extract_root_fix_ticket(regex: str):
    return select(
        order.root_fix_ticket
        for order in Missing_Reason
        if order.error_message == regex
    )[:]


@db_session
def extract_comment(order_id_info):
    return Missing_Reason.get(order_id=order_id_info).comment


@db_session
def select_new_failed_order():
    return select(
        order.order_id
        for order in Missing_Reason
        if (order.repush_ticket == None or order.repush_ticket == "")
        and (order.root_fix_ticket == None or order.root_fix_ticket == "")
        and (order.no_show == None or order.no_show == 0)
    ).order_by(-1)[:]


def create_missing_order(
    order_id,
    repush_ticket="",
    root_fix_ticket="",
    comment="",
    error_message="",
    no_show=None,
):
    try:
        with db_session:
            Missing_Reason(
                order_id=order_id,
                repush_ticket=repush_ticket,
                root_fix_ticket=root_fix_ticket,
                comment=comment,
                error_message=error_message,
                no_show=no_show,
            )
            commit()
    except:
        logging.error("error inserting order {} into database!".format(order_id))
        pass


@db_session
def update_missing_order(
    order_id,
    repush_ticket="",
    root_fix_ticket="",
    comment="",
    error_message="",
    no_show=None,
):
    order = Missing_Reason.get(order_id=order_id)
    order.repush_ticket = repush_ticket
    order.root_fix_ticket = root_fix_ticket
    order.comment = comment
    order.error_message = error_message
    order.no_show = no_show


if __name__ == "__main__":
    print(select_new_failed_order())
    # update_missing_order(
    #     5577035,
    #     "AP-6418",
    #     "AP-4684",
    #     "Repush - Order rejected by Zen due to missing lastname",
    #     "ERROR.*lastName.*is a required field and must have a non empty value",
    # )
