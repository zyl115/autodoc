import requests as req
import json
import config as cfg
import logging

base_url = cfg.wibot["base_url"]
credentials = {"user": cfg.wibot["user"], "password": cfg.wibot["password"]}
auth_res = req.post("{}/login".format(base_url), data=credentials).json()
header = {
    "X-Auth-Token": auth_res["data"]["authToken"],
    "X-User-Id": auth_res["data"]["userId"],
}

channel_res = req.get(
    "{}/channels.info".format(base_url),
    params={"roomName": "wibotspam"},
    headers=header,
).json()
room_id = channel_res["channel"]["_id"]


def wibot_post_message(text):
    post_message_payload = {"roomId": room_id, "channel": "#wibotspam", "text": text}
    post_message_res = req.post(
        "{}/chat.postMessage".format(base_url),
        data=post_message_payload,
        headers=header,
    ).json()
    return post_message_res["success"]


def wibot_extract_log(order_id):
    if wibot_post_message("wibot cerebro2 extract {} to s3".format(order_id)):
        logging.info("Successfully sent message to wibot for order {}".format(order_id))
        return True
    else:
        logging.warning("Failed to send message to wibot for order {}".format(order_id))
        return False


# wibot_extract_log('')
