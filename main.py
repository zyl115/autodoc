import logging
import re
from datetime import date, datetime
import os.path
from os import path
import csv
import textwrap
import sys
from time import sleep
import time

from read_csv import get_failed_orders_id
from jira_read import *
from pony_orm import *
from jira import JIRA
from wibot import *
from s3 import *
from smtp import send_attachments
import config as cfg

#filenames

autodoc_log_filename = "autodoc_logs/{}.log".format(datetime.now().strftime("%Y-%b-%d_%H-%M-%S"))
autodoc_report_filename = "autodoc_reports/{}.csv".format(datetime.now().strftime("%Y-%b-%d_%H-%M-%S"))

# set up
if not os.path.exists("autodoc_logs"):
    os.makedirs("autodoc_logs")

logging.getLogger("googleapiclient.discovery_cache").setLevel(logging.ERROR)
logging.basicConfig(
    filename=autodoc_log_filename,
    filemode="w",
    format="%(asctime)s - %(message)s",
    level=logging.INFO,
)

if not os.path.exists("autodoc_reports"):
    os.makedirs("autodoc_reports")

report_file = open(autodoc_report_filename, "w+")
report_writer = csv.writer(
    report_file, delimiter=",", quotechar='"', quoting=csv.QUOTE_MINIMAL
)
report_writer.writerow(["Order ID", "Repush Ticket", "Root Fix Ticket", "Error Message", "Status"])

CONST_TIMEOUT = 1800
CONST_WAITING_TIME = 0
CONST_EXPIRY_DAYS = 5
CONST_BASE_PATH = os.path.abspath(os.getcwd())

def main(debug: bool):
    order_logs_filenames = []
    failed_orders_id = select_new_failed_order()

    if not failed_orders_id:
        logging.info("No new failed orders!")
    else:
        failed_orders_id, failed_by_wibot = send_message_to_wibot(
            failed_orders_id, []
        )
        wait_for_logs(CONST_WAITING_TIME)
        failed_orders_id, failed_by_s3, order_logs_filenames = download_logs(
            failed_orders_id, []
        )
        failed_remaining = process_failed_orders_id(
            failed_orders_id, [], debug=debug
        )
        write_csv_wibot(failed_by_wibot)
        write_csv_s3(failed_by_s3)
        write_csv_remaining(failed_remaining)

        undocumented_error = failed_by_wibot + failed_by_s3 + failed_remaining
        logging.info("undocumented error: {}".format(undocumented_error))
        print("undocumented error: {}".format(undocumented_error))
        print(
            "Find the complete log in folder 'autodoc_logs'. Report in folder 'autodoc/reports'"
        )
    
    delete_expired_logs("autodoc_logs", CONST_EXPIRY_DAYS)
    delete_expired_logs("autodoc_reports", CONST_EXPIRY_DAYS)
    delete_expired_logs("order_logs", CONST_EXPIRY_DAYS)

    report_file.close()
    attachments = [autodoc_log_filename, autodoc_report_filename] + order_logs_filenames
    send_attachments(cfg.smtp['sender'], cfg.smtp['recipients'], cfg.smtp['password'], attachments)

def contains_error(error_list, log):
    for error_regex in error_list:
        if error_regex.startswith("!!"):
            error_regex_absent = error_regex[2:]
            regex_result = list(
                map(
                    lambda line: bool(
                        re.search(r"{}".format(error_regex_absent), line)
                    ),
                    log,
                )
            )
            if not any(regex_result):
                return (True, error_regex)
        else:
            regex_result = list(
                map(lambda line: bool(re.search(r"{}".format(error_regex), line)), log)
            )
            if any(regex_result):
                return (True, error_regex)
    return (False, None)


def find_email(log):
    for line in log:
        email = re.search('"email":".*?"', line)
        if email:
            return email[0]

    logging.warning("Email not found!")
    return "none"


def filter_failed_orders(failed_orders_id):
    filtered_failed_orders_id = list(failed_orders_id)
    for order_id in failed_orders_id:
        if select_order(order_id):
            logging.info(
                "Order {} is already in datalake. Skipping this order.".format(order_id)
            )
            filtered_failed_orders_id.remove(order_id)
    return filtered_failed_orders_id


def send_message_to_wibot(failed_orders_id, undocumented_error):
    new_failed_orders_id = list(failed_orders_id)
    for order_id in failed_orders_id:
        wibot_send_success = wibot_extract_log(order_id)
        if not wibot_send_success:
            undocumented_error.append(order_id)
            new_failed_orders_id.remove(order_id)
        time.sleep(5)
    return (new_failed_orders_id, undocumented_error)


def wait_for_logs(seconds):
    logging.info("waiting for logs to appear at s3 bucket...")
    sleep(seconds)


def download_logs(failed_orders_id, undocumented_error):
    remaining_failed_orders_id = list(failed_orders_id)
    filenames = []
    for order_id in failed_orders_id:
        order_log = download_log(order_id, CONST_TIMEOUT)
        if not order_log:
            logging.warning("Logs with order ID of {} not found".format(order_id))
            undocumented_error.append(order_id)
            remaining_failed_orders_id.remove(order_id)
        else:
            filenames.append(order_log)
    return (remaining_failed_orders_id, undocumented_error, order_log)


def match_ticket(tickets, order_id, order_email, ticket_type, debug:bool):
    today = date.today()
    selected_ticket = ""
    summary = ""
    jira = jira_auth()
    tickets_not_null = list(filter(None, tickets))
    for ticket in tickets_not_null:
        issue = jira.issue(ticket)
        if issue.fields.status != "Resolved":
            selected_ticket = ticket
    
    if tickets_not_null and selected_ticket == "" and not debug:
        selected_ticket = jira_clone(jira, tickets_not_null[0])
    
    if selected_ticket:
        issue = jira.issue(selected_ticket)
        summary = issue.fields.summary

        if not debug:
            description_new = " - orderId:{}, {} ({})\n{}".format(
                order_id,
                order_email,
                today.strftime("%d %b %Y"),
                issue.fields.description,
            )
            issue.update(fields={"description": description_new})
            logging.info(
                "Order {} appended to {} ticket {}: {}".format(
                    order_id, ticket_type, ticket, summary
                )
            )
            
        return selected_ticket, summary
        

    logging.warning(
        "{} ticket for order ID of {} not found.".format(ticket_type, order_id)
    )
    return "", ""


def process_failed_orders_id(failed_orders_id, undocumented_error, debug:bool):
    for order_id in failed_orders_id:
        order_log = open("order_logs/{}.log".format(order_id), "r").readlines()
        order_email = find_email(order_log)
        error_list = extract_error_list()
        errorFound, error_regex = contains_error(error_list, order_log)
        if errorFound:
            logging.info("error for order {} found: {}".format(order_id, error_regex))

            repush_tickets = extract_repush_ticket(error_regex)
            root_fix_tickets = extract_root_fix_ticket(error_regex)
            selected_repush_ticket, repush_summary = match_ticket(
                repush_tickets, order_id, order_email, "Repush", debug
            )
            selected_root_fix_ticket, root_fix_summary = match_ticket(
                root_fix_tickets, order_id, order_email, "Root fix", debug
            )

            if not selected_repush_ticket and not selected_root_fix_ticket:
                undocumented_error.append(order_id)
                logging.warning(
                    "Repush and root fix ticket not found for order {} and therefore not documented into database.".format(
                        order_id
                    )
                )
            else:
                comment = repush_summary if repush_summary else root_fix_summary
                
                if not debug:
                    update_missing_order(
                        order_id,
                        selected_repush_ticket,
                        selected_root_fix_ticket,
                        comment,
                        error_regex,
                    )
                report_writer.writerow([order_id, selected_repush_ticket, selected_root_fix_ticket, comment, "Error handled, both JIRA and datalaka are updated."])

        else:
            logging.warning("Fail to identify error for order {}.".format(order_id))
            undocumented_error.append(order_id)

    return undocumented_error

def write_csv_wibot (failed_orders):
    for orderId in failed_orders:
        report_writer.writerow([orderId, "N/A", "N/A", "N/A", "Order not handled due to wibot operation failure"])

def write_csv_s3 (failed_orders):
    for orderId in failed_orders:
        report_writer.writerow([orderId, "N/A", "N/A", "N/A", "Order not handled due to S3 operation failure"])

def write_csv_remaining(failed_orders):
    for orderId in failed_orders:
        error_log = open(
            "order_logs/{}.log".format(orderId), "r"
        ).readlines()
        for line in error_log:
            error_message = re.search(r'"error":".*?"', line)
            if error_message:
                report_writer.writerow(
                    [orderId, "N/A", "N/A", error_message[0].replace('"', " "), "No matching error detected. Please handle this manually."]
                )
                break
            else:
                error_message = re.search(r"error.*?\n", line)
                if error_message:
                    report_writer.writerow(
                        [
                            orderId,
                            "N/A",
                            "N/A",
                            textwrap.shorten(
                                error_message[0].replace('"', " "), width=400
                            ),
                            "No matching error detected. Please handle this manually",
                        ]
                    )
                break
        if not error_message:
            report_writer.writerow(
                [orderId, "N/A", "N/A", "N/A", "Error message not found in log file. Please handle this manually."]
            )


def delete_expired_logs(directory_name, expiry_age_days):
    logging.info("Cleaning up directory {}...".format(directory_name))
    now = time.time()
    if os.path.exists(directory_name):
        path = os.path.join(CONST_BASE_PATH, directory_name)
        for filename in os.listdir(path):
            if os.path.getmtime(os.path.join(path, filename)) < now - expiry_age_days * 86400:
                if os.path.isfile(os.path.join(path, filename)):
                    os.remove(os.path.join(path, filename))
                    logging.info("Removed file {}".format(filename))

if __name__ == "__main__":
    if len(sys.argv)>1 and sys.argv[1]=="debug":
        main(debug=True)
    else:
        main(debug=False)
    
