from jira import JIRA
import config as cfg


def jira_auth():
    username = cfg.jira["username"]
    password = cfg.jira["password"]
    options = {"server": cfg.jira["server"]}
    jira = JIRA(options, auth=(username, password))
    return jira


def jira_read(jira, issue_id):
    issue = jira.issue(issue_id)
    return issue

def jira_clone(jira, issue_id):
    issue = jira.issue(issue_id)
    issue_summary = issue.raw['fields']['summary']
    issue_type = issue.raw['fields']['issuetype']
    issue_project = issue.raw['fields']['project']
    issue_priority = issue.raw['fields']['priority']
    issue_team = issue.raw['fields']['customfield_10105']
    issue_description = "This is a clone of {}. {}".format(issue_id, issue_summary)
    jira_dict_convert = {
        'project': issue_project,
        'summary': issue_summary,
        'issuetype': issue_type,
        'customfield_10105': issue_team,
        'description': issue_description
    }
    issue_clone = jira.create_issue(jira_dict_convert)
    jira.create_issue_link("Cloners", issue_clone, issue)
    return issue_clone.key


if __name__ == "__main__":
    jira = jira_auth()
    issue = jira.issue("AP-5054")
    print(str(issue.fields.status))
    # jira = jira_auth()
    # issue = jira.issue("TC-250")
    # print(jira_clone(jira, "TC-250"))
    # for field_name in issue.raw['fields']:
    #     print ("Field:", field_name, "Value:", issue.raw['fields'][field_name])